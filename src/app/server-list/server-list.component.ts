import { Component, OnInit } from '@angular/core';
import { ServerStateService } from '../server-state.service';

@Component({
  selector: 'app-server-list',
  templateUrl: './server-list.component.html',
  styleUrls: ['./server-list.component.css']
})
export class ServerListComponent implements OnInit {
  allowNewServer = false;

  serverCreationStatus = 'No server created';

  serverName = '';
  serverWasCreated = false;



  color1 = 'green';
  color2 = 'khaki';

  // questo è il contatore dei click sul pulsante
  contatoreClick = 0;

  constructor(public serverState: ServerStateService) {

    console.log('ServerListComponent.constructor(): serverState=', serverState);

    setTimeout(() => {
      this.allowNewServer = true;
    }, 5000);
  }

  ngOnInit(): void {
  }

  onCreateServer() {
    // this.contatoreClick = this.contatoreClick + 1;
    this.contatoreClick += 1;
    if (this.contatoreClick % 2 === 0) {
      this.color1 = 'green';
      this.color2 = 'khaki';
    } else {
      this.color1 = 'red';
      this.color2 = 'lightblue';
    }

    console.log('Chiamato: onCreateServer(): ', this.contatoreClick);

    this.serverWasCreated = true;

    this.serverState.addServer(this.serverName);
    this.serverCreationStatus = 'Server created!';
  }

  /**
   * Usata solamente quando la gestione dell'evento input è esplicita com:
   *   <input
   *       type="text"
   *       class="form-control"
   *       (input)="onUpdateServerName($event)"
   *   >
   *
   * ma diventa inutile (la puoi cancellare!) se viene gestito tramite ngModel:
   *
   * <input
   *    type="text"
   *    class="form-control"
   *    [(ngModel)]="serverName"
   * >
   *
   * @param event il parametro fornito dall'evento (input)
   */
  onUpdateServerName(event: Event) {
    console.log(event);

    this.serverName = (event.target as HTMLInputElement).value;

    //        (<HTMLInputElement>event.target).value;

    /*
    // nuova sintassi a partire da Typescript >= 1.6.x
    this.serverName =
            (event.target as HTMLInputElement).value;
    */
  }


  /**
   * Determino se il nome del server finise con un numero pari o dispari
   *
   * server può essere di questo tipo:
   *  - 'Server 1'
   *  - 'Server 2'
   *  - 'Server 3'
   *  - 'Server 13'
   *  - 'Il mio Server 13'
   *  - 'Il tuo Server 14'
   *  - 'Questo non ha numero'
   *  - 'Questo ha numero con più spazi     24'
   *  - ''
   *  - null
   *
   * Possibile soluzione:
   * 1. separo la stringa per gli spazi ed ottengo
   *    l'elenco delle parole
   *
   * 2. prendo dall'elenco l'ultima parola
   *
   * 3. la converto in numero
   *
   * 4. determino se il numero è pari o dispari
   *
   * @param server nome del server
   */
  haNomePari(server: string): boolean {
    let result = true;

    // 1. separo la stringa per gli spazi ed ottengo l'elenco delle parole
    const elencoParole = server.split(' ');
    console.log('haNomePari(): elencoParole=', elencoParole);

    // 2. prendo dall'elenco l'ultima parola
    const ultimaParola = elencoParole[elencoParole.length - 1];
    console.log('haNomePari(): ultimaParola=', ultimaParola);

    // come esercizio, 1+2: estraggo l'ultima parola da server
    // utilizzando indexOf()

    // 3. la converto in numero
    const numero = Number(ultimaParola);

    // 4. determino se il numero è pari o dispari
    if (numero % 2 !== 0) {
      result = false;
    } else {
      result = true;
    }

    return result;
  }



}
