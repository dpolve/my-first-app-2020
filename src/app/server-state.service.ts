import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ServerStateService {

  private serverList = [
    { id: this.generaUnID(), name: 'Server 1' },
    { id: this.generaUnID(), name: 'Server 2' },
    { id: this.generaUnID(), name: 'Server 3' },
    { id: this.generaUnID(), name: 'Questo non ha numero' },
    { id: this.generaUnID(), name: 'Questo ha numero con più spazi     24' },
  ];


  constructor() {
    console.log('ServerStateService.constructor()');
  }


  getServerList() {
    return this.serverList;
  }
  setServerList(newServerList) {
    this.serverList = newServerList;
  }

  /**
   * Crea un nuovo server e lo aggiunge nella lista
   * @param name nome del nuovo server
   */
  addServer(name: string) {
    const nuovoServer = {
      id: this.generaUnID(),
      name: name
    };
    this.serverList.push(nuovoServer);
  }

  /**
   * Rimuove un server dalla lista
   */
  removeServer(name: string) {
    for (let i = 0; i < this.serverList.length; i++) {
      const server = this.serverList[i];
      if (server.name === name) {
        this.serverList.splice(i, 1);
        break;
      }
    }
  }


  /**
   * Sposta la posizione del server in alto
   * @param name nome del server
   */
  upServer(name: string) {
    // 1. cerco la posizione di serverNameUp
    const indiceServerUp = this.getIndexOf(name);

    // ora indiceServerUp dovrebbe contenere l'indice
    // del server che devo scambiare

    // 2. se sono in una posizione (indice) che consente di andare "su"
    //    (cioé ridurre l'indice di 1):
    if (indiceServerUp > 0) {

      // 2.1. scambio la posizione con quello che sta in indice-1
      const serverUp = this.serverList[indiceServerUp];
      const serverUpMenoUno = this.serverList[indiceServerUp - 1];
      this.serverList[indiceServerUp] = serverUpMenoUno;
      this.serverList[indiceServerUp - 1] = serverUp;
    }
  }

  /**
   * Sposta la posizione del server in basso
   * @param name nome del server
   */
  downServer(name: string) {
    // 1. cerco la posizione di serverNameUp
    const indiceServerDown = this.getIndexOf(name);

    // ora indiceServerUp dovrebbe contenere l'indice
    // del server che devo scambiare

    // 2. se sono in una posizione (indice) che consente di andare "giù"
    //    (cioé aumentare l'indice di 1):
    if (indiceServerDown < this.serverList.length - 1) {

      // 2.1. scambio la posizione con quello che sta in indice+1
      const serverDown = this.serverList[indiceServerDown];
      const serverDownPiuUno = this.serverList[indiceServerDown + 1];
      this.serverList[indiceServerDown] = serverDownPiuUno;
      this.serverList[indiceServerDown + 1] = serverDown;
    }
  }


  /**
   * genera un id casuale tra 0 e 100 per assegnarlo
   * ai <app-server> tramite property binding
   */
  generaUnID(): number {
    return Math.floor(Math.random() * 101);
  }


  /**
   * Fornisce l'indice del server il cui nome è fornito come parametro
   * se non viene trovato il server ritorna -1
   * @param serverName nome del server da cercare
   */
  getIndexOf(serverName: string): number {
    // 1. cerco la posizione di serverName
    let indiceServer = -1;
    for (let indice = 0; indice < this.serverList.length; indice++) {
      const server = this.serverList[indice];
      if (server.name === serverName) {
        indiceServer = indice;
        break;
      }
    }
    // ora indiceServerUp dovrebbe contenere l'indice (oppure -1 se non trovato)
    return indiceServer;
  }


}
