import { Component, Input, OnInit } from '@angular/core';
import { ServerStateService } from '../server-state.service';
@Component({
    selector: 'app-server',
    templateUrl: './server.component.html',
    styles: [
      `.online {
        color: white;
      }

      button:disabled {
        background-color: gray;
      }
      `
    ]
})
export class ServerComponent implements OnInit {
  @Input() serverId: number = 0;

  serverStatus: string = 'offline';

  // Per consentire l'immissione del nome del server
  // tramite property binding:
  // <app-server ... [serverName]="server"></app-server>
  @Input() serverName: string = '';

  onlineColor = 'green';

  private questaNonLaVedo = 'boh?';

  constructor(public serverState: ServerStateService) {
    this.serverStatus = (Math.random() > 0.5 ? 'online' : 'offline');

  }


  /**
   * https://angular.io/guide/lifecycle-hooks
   *
   * Chiamato dopo il constructor(), dopo l'inizializzazione delle veriabili @Input() e dopo la
   * prima chiamata a ngOnChanges()
   */
  ngOnInit(): void {
    console.log('ServerComponent: serverName=', this.serverName);
  }

  getServerStatus() {
    // console.log('getServerStatus()');
    return this.serverStatus;
  }

  getColor() {
    return (this.serverStatus === 'online' ? this.onlineColor : 'red');
  }


  onReboot() {
    this.serverStatus = 'offline';
    setTimeout(() => {
      this.serverStatus = 'online';
    }, 2000);

    // Nel caso si voglia anche resettare il colore 'online' al reboot
    // this.onlineColor = 'green';
  }

  onRemove() {
    console.log('ServerComponent.onRemove()');
    this.serverState.removeServer(this.serverName);
  }

  onFrecciaSu() {
    console.log('ServerComponent.onFrecciaSu()');
    this.serverState.upServer(this.serverName);

  }

  onFrecciaGiu() {
    console.log('ServerComponent.onFrecciaGiu()');
    this.serverState.downServer(this.serverName);
  }

  onChangeColor_con_nomi() {
    const elencoColori = [
      'Aquamarine', 'Aqua', 'Blue', 'CadetBlue', 'Chartreuse',
      'CornflowerBlue', 'Cyan', 'green'
    ];

    // da 0 a 6.9999
    const numeroCasuale = Math.random() * elencoColori.length;
    // prendo la parte intera
    const indice = Math.floor(numeroCasuale);
    this.onlineColor = elencoColori[indice];
  }

  onChangeColor() {
    // detrmino a caso le componenti rosso, verde, blu del colore
    const r = Math.floor(Math.random() * 256);
    const g = Math.floor(Math.random() * 256);
    const b = Math.floor(Math.random() * 256);

    // costruisco la stringa del colore css
    // this.onlineColor = '#' + r.toString(16)
    //                        + g.toString(16)
    //                        + b.toString(16);
    this.onlineColor = 'rgb(' + r.toString(10)
                       + ','  + g.toString(10)
                       + ','  + b.toString(10)
                       + ')';
    console.log('onChangeColor(): ', this.onlineColor);
  }
}
